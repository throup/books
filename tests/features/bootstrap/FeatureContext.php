<?php
use Behat\Behat\Context\Step\Given;
use Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;

require_once(dirname(dirname(dirname(__DIR__))) . '/src/bootstrap.php');

class FeatureContext extends MinkContext {
    public function __construct() {
        $this->catalogue = new Catalogue();
    }

    /**
     * @Given /^the following books exist:$/
     */
    public function theFollowingBooksExist(TableNode $table) {
        foreach ($table->getHash() as $row) {
            $book = new Book();
            $book->setTitle($row['title']);
            $book->setAuthor($row['author']);
            $book->setIsbn($row['isbn']);
            $this->catalogue->addBook($book);
        }
    }

    /**
     * @When /^I view the book catalogue$/
     */
    public function iViewTheBookCatalogue() {
        return new Given('I am on "/catalogue.php"');
    }

    /**
     * @Then /^I should see the following books in order:$/
     */
    public function iShouldSeeTheFollowingBooksInOrder(TableNode $table) {
        $givens = [];
        foreach ($table->getHash() as $row) {
            $givens[] = new Given('I should see "' . $row['title'] . '"');
            $givens[] = new Given('I should see "' . $row['author'] . '"');
            $givens[] = new Given('I should see "' . $row['isbn'] . '"');
        }
        return $givens;
    }
}
