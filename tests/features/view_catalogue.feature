Feature: view a book catalogue
  In order to know what books are available
  As a user
  I need to be able to view the contents of the book catalogue

  Background:
    Given the following books exist:
           | title         | author         | isbn       |
           | Alien Bodies  | Lawrence Miles | 0563405775 |
           | Where's Spot? | Eric Hill      | 072326340X |
           | Animal Farm   | George Orwell  | 0452284244 |

  Scenario: viewing the catalogue in alphabetical order
     When I view the book catalogue
     Then I should see the following books in order:
           | title         | author         | isbn       |
           | Alien Bodies  | Lawrence Miles | 0563405775 |
           | Animal Farm   | George Orwell  | 0452284244 |
           | Where's Spot? | Eric Hill      | 072326340X |
