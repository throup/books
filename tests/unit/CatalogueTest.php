<?php
class CatalogueTest extends PHPUnit_Framework_TestCase {
    /**
     * @before
     */
    public function setUp() {
        $this->catalogue = new Catalogue();
    }

    /**
     * @test
     */
    public function addBook_acceptsInstanceOfBookClass() {
        $book = new Book();
        $this->catalogue->addBook($book);
    }

    /**
     * @test
     * @expectedException PHPUnit_Framework_Error
     */
    public function addBook_doesNotAcceptObjectNotOfBookClass() {
        $notABook = $this->getMock('NotABook');
        $this->catalogue->addBook($notABook);
    }

    /**
     * @test
     */
    public function noBooksAdded_getBooksReturnsEmptyArray() {
        $this->assertEquals([], $this->catalogue->getBooks());
    }

    /**
     * @test
     */
    public function givenABook_getBooksReturnsArray_containingThatBook() {
        $book = new Book();
        $this->catalogue->addBook($book);
        $books = $this->catalogue->getBooks();
        $this->assertContains($book, $books);
    }

    /**
     * @type Catalogue
     */
    private $catalogue;
}
