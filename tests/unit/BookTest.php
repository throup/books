<?php
class BookTest extends PHPUnit_Framework_TestCase {
    const BOOK_AUTHOR = 'Book author';
    const BOOK_ISBN = '01234567890';
    const BOOK_TITLE = 'Book title';

    /**
     * @before
     */
    public function setUp() {
        $this->book = new Book();
    }

    /**
     * @test
     */
    public function setTitle_returnedByGetTitle() {
        $title = self::BOOK_TITLE;
        $this->book->setTitle($title);
        $this->assertEquals($title, $this->book->getTitle());
    }

    /**
     * @test
     */
    public function setAuthor_returnedByGetAuthor() {
        $author = self::BOOK_AUTHOR;
        $this->book->setAuthor($author);
        $this->assertEquals($author, $this->book->getAuthor());
    }

    /**
     * @test
     */
    public function setIsbn_returnedByGetIsbn() {
        $isbn = self::BOOK_ISBN;
        $this->book->setIsbn($isbn);
        $this->assertEquals($isbn, $this->book->getIsbn());
    }

    /**
     * @type Book
     */
    private $book;
}
