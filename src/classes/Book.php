<?php
class Book {
    /**
     * @param string $title
     */
    public function setTitle($title) {
        $this->title = (string) $title;
    }

    /**
     * @return string
     */
    public function getTitle() {
        return (string) $this->title;
    }

    /**
     * @param string $author
     */
    public function setAuthor($author) {
        $this->author = (string) $author;
    }

    /**
     * @return string
     */
    public function getAuthor() {
        return (string) $this->author;
    }

    /**
     * @param string $isbn
     */
    public function setIsbn($isbn) {
        $this->isbn = (string) $isbn;
    }

    /**
     * @return string
     */
    public function getIsbn() {
        return (string) $this->isbn;
    }

    /**
     * @type string
     */
    private $title = '';

    /**
     * @type string
     */
    private $author = '';

    /**
     * @type string
     */
    private $isbn = '';
}
