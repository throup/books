<?php
class Catalogue {
    /**
     * @param Book $book
     */
    public function addBook(Book $book) {
        $this->books[] = $book;
    }

    /**
     * @return array|Book[]
     */
    public function getBooks() {
        return $this->books;
    }

    /**
     * @type array|Book[]
     */
    private $books = [];
}
