<?php
require_once('bootstrap.php');

$BOOKS = [
    [
        'title'  => 'Alien Bodies',
        'author' => 'Lawrence Miles',
        'isbn'   => '0563405775',
    ],
    [
        'title'  => 'Where\'s Spot?',
        'author' => 'Eric Hill',
        'isbn'   => '072326340X',
    ],
    [
        'title'  => 'Animal Farm',
        'author' => 'George Orwell',
        'isbn'   => '0452284244',
    ],
];

require_once('views/catalogue.view');
