<?php
spl_autoload_register(function($classname) {
    $filename = __DIR__ . "/classes/{$classname}.php";
    if (file_exists($filename)) {
        require_once($filename);
    }
});
