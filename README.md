# Book Catalogue

This is a PHP experiment to demonstrate building a web application, from the ground up, using Acceptance Test (with Behat) and
Unit Test (with PHPUnit) Driven Development.

At the time of writing, there is very little functionality but the commit history shows the evolution of the project, from
configuring the tools for the environment to defining the initial tests and functionality.

## Setting up a development environment

The repository contains a `composer.json` file to aid the installation of the necessary development tools.

To set up a development environment, follow these steps:

```
#!sh
$ git clone https://bitbucket.org/throup/books.git
$ cd books
$ curl -sS https://getcomposer.org/installer | php
$ php composer.phar install
```

## Running the tests

### Unit tests

There is a PHPUnit configuration file at `config/phpunit.xml`. To run the complete test unit test suite, execute:

```
#!sh
$ vendor/bin/phpunit -c config/phpunit.xml
```

### Acceptance tests

The acceptance test suite requires a web server running PHP 5.4+. The PHP executable itself can be used for this purpose.

There is a Behat configuration file at `config/behat.yml`. The default configuration assumes that the contents of the `src`
directory will be served as http://localhost:8080.

To start the built-in PHP server, execute:

```
#!sh
$ cd src
$ php -S localhost:8080
```

To run the complete acceptance test suite, execute (in a separate shell):

```
#!sh
$ vendor/bin/behat
```

### All test suites

There is a Phing build script at `build.xml` which defines targets for all of the test suites.

```
#!sh
$ phing phpunit
# executes PHPUnit unit test suites

$ phing behat
# executes Behat acceptance test suites

$ phing     # (or phing build)
# executes both unit and acceptance test suites

$ phing behat-ci
# executes Behat acceptance test suites, output log files for use by Continuous Integration servers (eg Jenkins)
```

